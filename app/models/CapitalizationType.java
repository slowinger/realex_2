package models;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;

/**
 * Created by slowinger on 7/20/15.
 */
@Entity
public class CapitalizationType extends Model
{
    public static Find<String, CapitalizationType> find = new Finder<>(CapitalizationType.class);

    @Id
    public Integer debtType;
    public String debtName;

    public static List<CapitalizationType> findAll() {
        return find.orderBy("debtType").findList();
    }


    public static String findDebt(Integer nDebtType) {
        List<CapitalizationType> capitalizationTypes = findAll();
        for (CapitalizationType capitalizationType : capitalizationTypes)
        {
            if (nDebtType.equals(capitalizationType.debtType)) {
                return capitalizationType.debtName;
            }
        }
        return null;
    }
}
