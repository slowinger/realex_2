package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;

/**
 * Created by slowinger on 7/2/15.
 */
@Entity
public class Sponsor extends Model
{

    public static Find<String, Sponsor> find = new Finder<>(Sponsor.class);

    @Id
    public Integer sponsorId;
    public String name;
    public String website;
    public String address;
    public String description;

    public static List<Sponsor> findAll() {
        return find.orderBy("sponsorId").findList();
    }

    public static Sponsor findById(int nId) {
        List<Sponsor> sponsors = findAll();
        for (Sponsor sponsor : sponsors) {
            if (nId == sponsor.sponsorId) {return sponsor;}
        }
        return null;
    }
    public static PagedList<Sponsor> page(int page, int pageSize, String sortBy, String order, String filter) {
        return
                find.where()
                        .ilike("name", "%" + filter + "%")
                        .orderBy(sortBy + " " + order)
                        .findPagedList(page, pageSize);
    }
}
