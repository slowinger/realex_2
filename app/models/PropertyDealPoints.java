package models;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by slowinger on 8/19/15.
 */
@Entity
public class PropertyDealPoints extends Model
{
    public Integer propertyId;
    public String name;
    public String description;

    public static Find<String, PropertyDealPoints> find = new Finder<>(PropertyDealPoints.class);

    public static List<PropertyDealPoints> findByPropertyId(int nId) {
        return find.findList().stream()
                .filter(p -> p.propertyId == nId)
                .collect(Collectors.toList());
    }
}
