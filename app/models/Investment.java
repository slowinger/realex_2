package models;

import com.avaje.ebean.Model;
import fundAmerica.EntityResults;
import fundAmerica.InvestmentResults;
import org.joda.time.format.ISODateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PersistenceException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by slowinger on 7/29/15.
 */
@Entity
public class Investment extends Model
{
    @Id
    public String id;
    public String user;
    public Integer propertyId;
    public Integer shares;
    public Date date;
    public Double notional;

    // Constructors
    public Investment(String strId, String strUser, Integer nPropertyId, Integer nShares, Date date) {
        this.id = strId;
        this.user = strUser;
        this.propertyId = nPropertyId;
        this.shares = nShares;
        this.date = date;

        final Property property = Property.findById(nPropertyId);

        if (property != null) {
            this.notional = ((double) nShares) *
                    (property.offeringSize / property.initialShares);
        } else {this.notional = null;}
    }

    //Default Constructor
    public Investment() {}

    public static Find<String, Investment> find = new Finder<>(Investment.class);

    public static List<Investment> findAll() {
        return find.orderBy("propertyId").findList();
    }


//    @Override
//    public boolean equals(Object obj){
//
//    }


    public static Investment findById(String strId) {
        return findAll().stream()
                .filter(i -> i.id.equals(strId))
                .findFirst()
                .get();
    }

    public static List<Investment> findByUser(String strEmail) {
        return findAll().stream()
                .filter(i -> i.user.equals(strEmail))
                .collect(Collectors.toList());
    }

    public static Set<Investment> findDistinctByUser(String strEmail) {
        return findAll().stream()
                .filter(i -> i.user.equals(strEmail))
                .collect(Collectors.toSet());
    }

    public static List<Investment> findDistinctByUserList(String strEmail, Date dLastAccessed) {
        return findAll().stream()
                .filter(i -> i.user.equals(strEmail))
                .filter(i -> i.date.after(dLastAccessed))
                .collect(Collectors.toList());
    }

    public static HashMap<Integer, Integer> getTotalInvestmentsByUser(String strEmail) {

        HashMap<Integer, Integer> hmInvestments = new HashMap();
        List<Investment> lInvestments = findByUser(strEmail);

        for (Investment investment : lInvestments) {
            if (hmInvestments.get(investment.propertyId) != null) {
                hmInvestments.put(investment.propertyId, hmInvestments.get(investment.propertyId) + investment.shares);
            }
            else {
                hmInvestments.put(investment.propertyId, investment.shares);
            }
        }
        return hmInvestments;
    }
    public static HashMap<Integer, Integer> getTotalInvestmentsBySponsorForUser(String strEmail) {

        HashMap<Integer, Integer> hmInvestments = new HashMap();
        List<Investment> lInvestments = findByUser(strEmail);

        for (Investment investment : lInvestments) {
            Integer nSponsorId = Property.findById(investment.propertyId).sponsorId;
            if (hmInvestments.get(nSponsorId) != null) {
                hmInvestments.put(nSponsorId, hmInvestments.get(nSponsorId) + investment.shares);
            }
            else {
                hmInvestments.put(nSponsorId, investment.shares);
            }
        }
        return hmInvestments;
    }

    public static void addInvestments(User user) {
        try {
            EntityResults.Resource resUser =
                    EntityResults.findEntity(user.email, Long.parseLong(user.phoneNumber.replaceAll("[^\\d.]", "")));

            String userId = resUser.id;

            List<InvestmentResults.Resource> userInvestments = InvestmentResults.findUserInvestments(userId, user.lastAccessed);

            for (InvestmentResults.Resource resInvestment : userInvestments) {
    //            System.out.println(resInvestment.entity_url);
                String strFundAmericaId = resInvestment.offering_url.split("offerings/")[1];
                System.out.println(strFundAmericaId);
                Property propSelected = Property.findByFundAmericaId(strFundAmericaId);

                org.joda.time.format.DateTimeFormatter parser = ISODateTimeFormat.dateTime();
                Date dInvestment = parser.parseDateTime(resInvestment.created_at).toDate();

                Investment invNew = new Investment(resInvestment.id, user.email, propSelected.propertyId, (int) resInvestment.equity_share_count, dInvestment);
                try {invNew.save();} catch (PersistenceException pe) {
                    pe.printStackTrace();
                }
            }

            user.lastAccessed = Calendar.getInstance().getTime();
            user.save();
        } catch (NullPointerException ne) {System.out.println("User doesnt exist");}
    }

}
