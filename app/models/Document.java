package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.EnumValue;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by slowinger on 9/12/15.
 */
@Entity
public class Document extends Model {

    public enum Format {
        @EnumValue("pdf")
        PDF,

        @EnumValue("doc")
        DOC
    }

    @Id
    public Long id;
    public String name;
    public Long userId;
    public Integer propertyId;
    public Date createTime;
    public boolean wasRead;
    public Format format;

    public static Model.Find<String, Document> find = new Model.Finder<>(Document.class);
    public static List<Document> findAll() {
        return find.orderBy("createTime").findList();
    }

    public static List<Document> findByUser(long nUserId) {
        return findAll().stream()
                .filter(d -> d.userId == nUserId)
                .sorted((d1, d2) -> d2.createTime.compareTo(d1.createTime))
                .collect(Collectors.toList());
    }

    public static Document findById(long nId) {
        return findAll().stream()
                .filter(d -> d.id == nId)
                .findFirst()
                .orElse(null);
    }

    public static long countUnreadByUser(long nUserId) {
        return findAll().stream()
                .filter(d -> d.userId == nUserId)
                .filter(d -> !d.wasRead)
                .count();
    }

    public static void markAsRead(Document readDocument) {
        if (!readDocument.wasRead) {
            readDocument.wasRead = true;
            readDocument.save();
        }
    }

    @Override
    public String toString() {
        final Property propertySelected = Property.findById(propertyId);
        if (propertySelected != null) {
            final String strPropertyName = propertySelected.name;
            return name + "-" + strPropertyName;
        } else {return name;}
    }
}
