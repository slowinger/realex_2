package models;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import java.util.Date;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;

/**
 * Created by slowinger on 8/6/15.
 */
@Entity
public class RateReturn extends Model
{
    public Integer propertyId;
    public Date term;
    public Double estimateReturn;
    public Double actualReturn;

    public static Find<String, RateReturn> find = new Finder<>(RateReturn.class);

    public static List<RateReturn> findAll() {
        return find.orderBy("term").findList();
    }

    public static List<RateReturn> findByUser(String strUser) {
        List<Integer> investments = Investment.findDistinctByUser(strUser).stream()
                .map(i -> i.propertyId)
                .collect(Collectors.toList());
        List<RateReturn> returns = RateReturn.findAll();
        return returns.stream()
                .filter(r -> investments.contains(r.propertyId))
                .collect(Collectors.toList());
    }

    public static OptionalInt findFirstId(List<RateReturn> rateReturns) {
        return rateReturns.stream()
                .mapToInt(r -> r.propertyId)
                .min();
    }
}
