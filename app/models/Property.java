package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by slowinger on 8/28/15.
 */
@Entity
public class Property extends Model
{
    public static Model.Find<String, Property> find = new Model.Finder<>(Property.class);

    @Id
    public Integer propertyId;
    public String name;
    public String address;
    public String city;
    public String state;
    public Integer offeringSize;
    public Integer sharesAvailable;
    public String summary;
    public String description;
    public String tenantSummary;
    public String marketSummary;
    public Integer sponsorId;
    public String grade;
    public String deal_type;
    public Date end_date;
    public Double irr;
    public Double rate;
    public Integer initialShares;
    public String link;
    public String fundAmericaId;
    public String capitalRating;
    public String locationRating;
    public String occupancyRating;
    public String sponsorRating;
    public String personalGuaranteeRating;
    public String propertyGuaranteeRating;
    public Integer images;





    public static List<Property> findAll() {
        return find.orderBy("propertyId").findList();
    }

    public static Property findById(int nId) {
        List<Property> properties = findAll();
        for (Property property : properties)
        {
            if (nId == property.propertyId) {
                return property;
            }
        }
        return null;
    }

    public static Property findByFundAmericaId(String strFundAmericaId) {
        //System.out.println("Explicit Parameter: " + strFundAmericaId);

        return findAll().stream()
                .filter(p -> p.fundAmericaId.equals(strFundAmericaId))
                .findFirst()
                .get();
    }

    public static List<Property> findBySponsorId(int nSponsorId) {
        System.out.println("explicit parameter: " + nSponsorId);


        return findAll().stream()
                .filter(p -> p.sponsorId != null)
                .filter(p -> p.sponsorId == nSponsorId)
                .collect(Collectors.toList());
    }

    public static PagedList<Property> page(int page, int pageSize, String sortBy, String order, String filter) {
        return
                find.where()
                        .ilike("name", "%" + filter + "%")
                        .orderBy(sortBy + " " + order)
                        .findPagedList(page, pageSize);
    }
}
