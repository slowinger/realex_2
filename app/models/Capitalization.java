package models;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import java.util.List;

/**
 * Created by slowinger on 7/20/15.
 */
@Entity
public class Capitalization extends Model
{
    public static Find<String, Capitalization> find = new Finder<>(Capitalization.class);


    public Integer propertyId;
    public Integer debtType;
    public String source;
    public Double amount;

    public static List<Capitalization> findAll() {
        return find.orderBy("propertyId").findList();
    }


    public static List<Capitalization> findByProperty(Integer nPropertyId) {
        return find
                .where()
                .eq("propertyId", nPropertyId)
                .findList();
    }
}
