package googleMapsApi;


import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;

/**
 * Created by slowinger on 8/21/15.
 */
public class Geocode {

    public static LatLng getResponse(String strAddress) {
        GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyA_x1DRkV2y0BOELO4-HLd7nl0RtFBHEYk");

        GeocodingResult[] results = new GeocodingResult[0];
        try {
            results = GeocodingApi.geocode(context, strAddress).await();
            return results[0].geometry.location;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }


}


