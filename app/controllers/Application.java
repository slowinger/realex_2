package controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;
import models.*;
import play.Routes;
import play.data.Form;
import play.mvc.*;
import play.mvc.Http.Session;
import play.mvc.Result;
import providers.MyUsernamePasswordAuthProvider;
import providers.MyUsernamePasswordAuthProvider.MyLogin;
import providers.MyUsernamePasswordAuthProvider.MySignup;

import views.html.*;
import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;

import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.providers.password.UsernamePasswordAuthProvider;
import com.feth.play.module.pa.user.AuthUser;

import static com.feth.play.module.mail.Mailer.getEmailName;

public class Application extends Controller {

	public static final Form<Contact> contactForm = Form.form(Contact.class);

	public static final String FLASH_MESSAGE_KEY = "message";
	public static final String FLASH_ERROR_KEY = "error";
	public static final String USER_ROLE = "user";
	
	public static Result index() {
		return ok(home.render(MyUsernamePasswordAuthProvider.LOGIN_FORM, false));
	}
	public static Result home() { return ok(home.render(MyUsernamePasswordAuthProvider.LOGIN_FORM, false));}

	public static User getLocalUser(final Session session) {
		final AuthUser currentAuthUser = PlayAuthenticate.getUser(session);
		final User localUser = User.findByAuthUserIdentity(currentAuthUser);
		return localUser;
	}

	public static Result about() {return ok(about.render(MyUsernamePasswordAuthProvider.LOGIN_FORM, false));}

	public static Result contact() {return ok(contact.render(MyUsernamePasswordAuthProvider.LOGIN_FORM, contactForm, false, null));}

	public static Result doContact() {
		Contact contact = contactForm.bindFromRequest().get();

		try {
			MyUsernamePasswordAuthProvider.getProvider().sendContactEmail(contact);
			return ok(views.html.contact.render(MyUsernamePasswordAuthProvider.LOGIN_FORM, contactForm, false, "Success"));
		} catch (Exception e) {
			return ok(views.html.contact.render(MyUsernamePasswordAuthProvider.LOGIN_FORM, contactForm, false, "Fail"));
		}
	}

	public static Result faq() {
		return ok(faq.render(MyUsernamePasswordAuthProvider.LOGIN_FORM, false));
	}


	@Restrict(@Group(Application.USER_ROLE))
	public static Result restricted() {
		final User localUser = getLocalUser(session());
		return ok(restricted.render(localUser));
	}

	@Restrict(@Group(Application.USER_ROLE))
	public static Result profile() {
		final User localUser = getLocalUser(session());
		return ok(profile.render(localUser));
	}

	@Restrict(@Group(Application.USER_ROLE))
	public static Result investmentDocuments() {
		final User localUser = getLocalUser(session());
		final List<Document> lDocuments = Document.findByUser(localUser.id);
		return ok(documents.render(localUser, lDocuments));
	}

	@Restrict(@Group(Application.USER_ROLE))
	public static Result viewDocument(Long lDocument) {
		final Document documentRequested = Document.findById(lDocument);
		final User requester = getLocalUser(session());

		Document.markAsRead(documentRequested);

		if (documentRequested != null) {
			if (documentRequested.userId == requester.id) {
				final String strFilePath = "app/documents/" + lDocument + ".pdf";
				final String strTitle =  documentRequested.toString().replace(",","");
				response().setHeader("Content-disposition", "inline; filename=" + strTitle + ".pdf" );
				return ok(new java.io.File(strFilePath)).as("application/pdf");
			} else {
				return unauthorized("forbidden");
			}
		} else {return notFound(pagenotfound.render());}
	}


	@Restrict(@Group(Application.USER_ROLE))
	public static Result account() {
		User user = getLocalUser(session());

		Investment.addInvestments(user);
		Set<Investment> investments = Investment.findDistinctByUser(user.email);
		List<RateReturn> returns = RateReturn.findByUser(user.email);
		List<Investment> userInvestments = Investment.findByUser(user.email);

		double totalInvested = userInvestments.stream()
				.filter(ui -> ui.notional != null)
				.mapToDouble(ui -> ui.notional)
				.sum();


		return ok(accountSummary.render(investments, returns, userInvestments, user.email, totalInvested));
	}

	public static Result login() {
		return ok(home.render(MyUsernamePasswordAuthProvider.LOGIN_FORM, true));
	}

	public static Result doLogin() {
		com.feth.play.module.pa.controllers.Authenticate.noCache(response());
		final Form<MyLogin> filledForm = MyUsernamePasswordAuthProvider.LOGIN_FORM
				.bindFromRequest();
		if (filledForm.hasErrors()) {
			// User did not fill everything properly
			return badRequest(home.render(filledForm, true));
		} else {
			// Everything was filled
			return UsernamePasswordAuthProvider.handleLogin(ctx());
		}
	}

	public static Result signup() {
		return ok(signup.render(MyUsernamePasswordAuthProvider.LOGIN_FORM, MyUsernamePasswordAuthProvider.SIGNUP_FORM, "", false));
	}


	public static Result jsRoutes() {
		return ok(
				Routes.javascriptRouter("jsRoutes",
						controllers.routes.javascript.Signup.forgotPassword()))
				.as("text/javascript");
	}

	public static Result doSignup() {
		com.feth.play.module.pa.controllers.Authenticate.noCache(response());
		final Form<MySignup> filledForm = MyUsernamePasswordAuthProvider.SIGNUP_FORM
				.bindFromRequest();

		if (filledForm.hasErrors()) {
			// User did not fill everything properly
			return badRequest(signup.render(MyUsernamePasswordAuthProvider.LOGIN_FORM, filledForm, "Bad Request", false));
		} else {
			// Everything was filled
			// do something with your part of the form before handling the user
			// signup
			return UsernamePasswordAuthProvider.handleSignup(ctx());
		}
	}

	public static String formatTimestamp(final long t) {
		return new SimpleDateFormat("yyyy-dd-MM HH:mm:ss").format(new Date(t));
	}


}