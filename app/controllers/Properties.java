package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import com.google.maps.model.LatLng;

import fundAmerica.InvestmentResults;
import googleMapsApi.Geocode;
import models.Capitalization;
import models.Property;
import models.Sponsor;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.properties;
import views.html.property;

import java.util.List;


/**
 * Created by slowinger on 7/2/15.
 */
public class Properties extends Controller
{
    public static final Form<Property> propertyForm = Form.form(Property.class);

    public Result list(){
        List<Property> properties = Property.findAll();
        //System.out.println(String.valueOf(allJobs));
        return ok(Json.toJson(properties));
    }

    public Result save()
    {
        //Extract data from the form
        Form<Property> newProperty = propertyForm.bindFromRequest();

        Property property = newProperty.get();

        //persist to database
        try {property.save();}
        catch (Exception e){
            return redirect("/home");
        }
        return redirect("/home");
    }

    @Restrict(@Group(Application.USER_ROLE))
    public Result showProperty(int nId) {

        Property propSelected = Property.findById(nId);
        Sponsor sponSelected = Sponsor.findById(propSelected.sponsorId);
        List<Capitalization> capsSelected = Capitalization.findByProperty(nId);

        //Call FundAmerica Api
        InvestmentResults.writeInvestments(propSelected);

        //Get Geocode
        LatLng propCoordinates = Geocode.getResponse(propSelected.address);

        double dFunded = ((double) propSelected.initialShares -  (double) propSelected.sharesAvailable) / (double) propSelected.initialShares;
        double dFundedDollar = dFunded * propSelected.offeringSize;
        dFunded *= 100;

        return ok(property.render(propSelected, propCoordinates, sponSelected, capsSelected, session().get("email"), dFunded, dFundedDollar));
    }



    @Restrict(@Group(Application.USER_ROLE))
    public Result listProperties(int page, String sortBy, String order, String filter) {
        return ok(
                properties.render(
                        Property.page(page, 9, sortBy, order, filter),
                        sortBy, order, filter, session().get("email")
                )
        );
    }

}
