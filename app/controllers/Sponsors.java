package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import models.Sponsor;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.sponsor;
import views.html.sponsors;

import java.util.List;

/**
 * Created by slowinger on 7/2/15.
 */
public class Sponsors extends Controller
{
    public static final Form<Sponsor> sponsorForm = Form.form(Sponsor.class);

    public Result list(){
        List<Sponsor> sponsors = Sponsor.findAll();
        //System.out.println(String.valueOf(allJobs));
        return ok(Json.toJson(sponsors));
    }

    public Result save()
    {
        //Extract data from the form
        Form<Sponsor> newSponsor = sponsorForm.bindFromRequest();

        Sponsor sponsors = sponsorForm.get();

        //persist to database
        try {sponsors.save();}
        catch (Exception e){
            return redirect("/home");
        }
        return redirect("/home");
    }


    @Restrict(@Group(Application.USER_ROLE))
    public Result viewSponsor(int nId) {
        Sponsor spnCall = getSponsor(nId);
       return ok(sponsor.render(spnCall));
    }

    @Restrict(@Group(Application.USER_ROLE))
    public Result listSponsors(int page, String sortBy, String order, String filter) {
        return ok(
                sponsors.render(
                        Sponsor.page(page, 9, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    private static Sponsor getSponsor(Integer nId) {
        return Sponsor.findById(nId);
    }
}
