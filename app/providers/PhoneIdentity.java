package providers;

/**
 * Created by slowinger on 9/11/15.
 */
public interface PhoneIdentity {
    public String getPhoneNumber();
}
