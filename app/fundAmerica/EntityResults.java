package fundAmerica;

import com.google.gson.Gson;

import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by slowinger on 8/11/15.
 */
public class EntityResults
{
    public List<Resource> resources;
    public class Resource {
        public String id;
        public String city;
        public String country;
        public String date_of_birth;
        public String email;
        public String name;
        public Long phone;
        public int postal_code;
        public String streed_address_1;
        public String streed_address_2;
        public Long tax_id_number;
    }

    public List<Resource> getResources() {
        return resources;
    }

    public static List<Resource> getAllEntities() {

        String strResult = null;
        try {
            strResult = HtttpConnect.readUrl("entities");
        } catch (Exception e) {e.printStackTrace();}

        Gson gson = new Gson();
        EntityResults results = gson.fromJson(strResult, EntityResults.class);
        return results.getResources();
    }

    public static Resource findEntity(String strEmail, Long lPhone) {
        List<Resource> entities = getAllEntities();

        System.out.println("email: " + strEmail);
        System.out.println("phone number: " + lPhone);

        try
        {
            return entities.stream()
                    .filter(r -> r.email.equals(strEmail))
//                .filter(r -> r.phone == lPhone)
                    .findFirst()
                    .get();
        } catch (NoSuchElementException e) {return null;}
    }

}
