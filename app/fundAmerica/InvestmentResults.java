package fundAmerica;

import com.google.gson.Gson;
import models.Property;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by slowinger on 8/10/15.
 */
public class InvestmentResults
{
    public List<Resource> resources;
    public class Resource {
        public double amount;
        public String offering_url;
        public double equity_share_count;
        public double equity_share_price;
        public String entity_url;
        public String id;
        public String created_at;
    }


    public List<Resource> getSimpleValues() {
        return resources;
    }

    public static void writeInvestments(Property propSelected) {

        String strResult = null;
        try {
            strResult = HtttpConnect.readUrl("investments");
        } catch (Exception e) {e.printStackTrace();}

        final String strFAId = propSelected.fundAmericaId;

        Gson gson = new Gson();
        InvestmentResults results = gson.fromJson(strResult, InvestmentResults.class);

        try {
            List<Resource> resources = results.getSimpleValues();

            double dRaised = resources.stream()
                    .filter(i -> i.offering_url.contains(strFAId))
                    .mapToDouble(i -> i.equity_share_count)
                    .sum();

            double dNewSharesAvailable = propSelected.initialShares - dRaised;

            if (propSelected.sharesAvailable >= dNewSharesAvailable) {
                propSelected.sharesAvailable = (int) dNewSharesAvailable;
                propSelected.save();
            }
        } catch (NullPointerException ne) {
            ne.printStackTrace();
        }

    }

    public static List<Resource> findUserInvestments(String strId, Date dStart) {
        String strResult = null;
        try {
            strResult = HtttpConnect.readUrl("investments");
        } catch (Exception e) {e.printStackTrace();}

        Gson gson = new Gson();
        InvestmentResults results = gson.fromJson(strResult, InvestmentResults.class);
        List<Resource> resources = results.getSimpleValues();

        DateTimeFormatter parser = ISODateTimeFormat.dateTime();

        if (dStart != null) {
            return resources.stream()
                    .filter(i -> i.entity_url.contains(strId))
                    .filter(i -> parser.parseDateTime(i.created_at).toDate().after(dStart))
                    .collect(Collectors.toList());
        }
        else {
            return resources.stream()
                    .filter(i -> i.entity_url.contains(strId))
                    .collect(Collectors.toList());
        }

    }

}
