package fundAmerica;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by slowinger on 8/10/15.
 */
public class HtttpConnect
{
    private static final String strFundAmericaKey = "BLrHyez7F1AmwTd0ZwMBFEcxqRSOQyEn";
    private static String strFundAmericaUrl = "https://sandbox.fundamerica.com/api/";

    public static String readUrl(String strPage) throws Exception {
        BufferedReader rd;
        StringBuilder sb;
        String line;
        try {
            URL obj = new URL(strFundAmericaUrl + strPage + "/");
            HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);

            conn.setRequestMethod("GET");

            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");

            //String userpass = "user" + ":" + "pass";
            String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(strFundAmericaKey.getBytes("UTF-8"));
            conn.setRequestProperty("Authorization", basicAuth);

            conn.connect();
            rd  = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            sb = new StringBuilder();

            while ((line = rd.readLine()) != null) {
                sb.append(line + '\n');
            }

            String strResult = sb.toString();

            return strResult;


        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
}
