organization := "com.realex"

name := "web_platform"

scalaVersion := "2.11.6"

version := "2"

val appDependencies = Seq(
  "be.objectify"  %% "deadbolt-java"     % "2.4.0",
  // Comment the next line for local development of the Play Authentication core:
  "com.feth"      %% "play-authenticate" % "0.7.0-SNAPSHOT",
  cache,
  javaWs,
  javaJdbc,
  "org.webjars" % "bootstrap" % "3.2.0",
  "org.easytesting" % "fest-assert" % "1.4" % "test",
  "mysql" % "mysql-connector-java" % "5.1.26",
  "com.typesafe.play" %% "play-mailer" % "3.0.1",
  "org.codeartisans" % "org.json" % "20131017",
  "com.google.maps" % "google-maps-services" % "0.1.7",
  "com.google.code.gson" % "gson" % "2.3.1"
)

// add resolver for deadbolt and easymail snapshots
resolvers += Resolver.sonatypeRepo("snapshots")

//  Uncomment the next line for local development of the Play Authenticate core:
//lazy val playAuthenticate = project.in(file("modules/play-authenticate")).enablePlugins(PlayJava)

lazy val root = project.in(file("."))
  .enablePlugins(PlayJava, PlayEbean)
  .settings(
    libraryDependencies ++= appDependencies
  )
  /* Uncomment the next lines for local development of the Play Authenticate core: */
  //.dependsOn(playAuthenticate)
  //.aggregate(playAuthenticate)

enablePlugins(JavaAppPackaging)



