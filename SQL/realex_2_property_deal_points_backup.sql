-- MySQL dump 10.13  Distrib 5.6.19, for osx10.7 (i386)
--
-- Host: 127.0.0.1    Database: realex_2
-- ------------------------------------------------------
-- Server version	5.6.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `property_deal_points_backup`
--

LOCK TABLES `property_deal_points_backup` WRITE;
/*!40000 ALTER TABLE `property_deal_points_backup` DISABLE KEYS */;
INSERT INTO `property_deal_points_backup` VALUES (1,'Lease Structure','Long-term absolute net lease'),(1,'Tenant Credit','Publicly traded tenant (NYSE: ATU / S&P: BB+'),(1,'Operating History','105 year old company headquartered within 20 miles of property since founding'),(1,'Return Profile','Strong current cash returns'),(1,'Asset Quality','Recently constructed Class A corporate headquarters'),(1,'Debt Structure','Long term fixed rate financing'),(2,'Lease','Long-term absolute net lease'),(2,'Returns','Opportunistic returns'),(2,'Location','Route 128 Technology Corridor'),(2,'Mortgage','Long-term fixed rate financing'),(3,'Value','Opportunistic purchase @ $66 per square foot'),(3,'Quality','Institutional-quality real estate'),(3,'Date','Built in 1989 by United Airlines as headquarters for its reservation system'),(3,'Opportunistic','Potential for significant value creation'),(3,'Cost','Purchase price substantially below replacement cost');
/*!40000 ALTER TABLE `property_deal_points_backup` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-15 13:49:49
