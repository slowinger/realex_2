-- MySQL dump 10.13  Distrib 5.6.19, for osx10.7 (i386)
--
-- Host: 127.0.0.1    Database: realex_2
-- ------------------------------------------------------
-- Server version	5.6.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `sponsor_backup`
--

LOCK TABLES `sponsor_backup` WRITE;
/*!40000 ALTER TABLE `sponsor_backup` DISABLE KEYS */;
INSERT INTO `sponsor_backup` VALUES (1,'Middleton Partners','http://www.asacre.com/','400 Skokie Blvd. Suite 405\nNorthbrook, IL 60062','ASA Properties/Middleton Partners, a private real estate investment firm, offers clients the benefits of over 30 years of partners\' commercial real estate experience. Since inception, ASA Properties/Middleton Partners has been involved in over $225 million in property acquisitions and redevelopments. We invest alongside our clients, ensuring a true partnership. \n\nASA Properties/Middleton Partners is a firm focused on value-added opportunities. Our size and experience allow us to identify under-managed assets or undervalued markets and apply our expertise to enhance the asset\'s performance. \n\nASA Properties/Middleton Partners invests for its own account, and on behalf of a select few like-minded partners. The list of services ASA Properties/Middleton Partners provides includes: Asset management, acquisitions and dispositions; Supervisory property management, leasing and construction; Under-performing equity and specialized asset management; Development.');
/*!40000 ALTER TABLE `sponsor_backup` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-15 13:49:49
