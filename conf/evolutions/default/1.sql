
# --- !Ups

create table capitalization (
  property_id               integer,
  debt_type                 integer,
  source                    varchar(255),
  amount                    double)
;

create table capitalization_type (
  debt_type                 integer auto_increment not null,
  debt_name                 varchar(255),
  constraint pk_capitalization_type primary key (debt_type))
;

create table document (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  user_id                   bigint,
  property_id               integer,
  create_time               datetime(6),
  was_read                  tinyint(1) default 0,
  format                    varchar(3),
  constraint ck_document_format check (format in ('pdf','doc')),
  constraint pk_document primary key (id))
;

create table investment (
  id                        varchar(255) not null,
  user                      varchar(255),
  property_id               integer,
  shares                    integer,
  date                      datetime(6),
  constraint pk_investment primary key (id))
;

create table linked_account (
  id                        bigint auto_increment not null,
  user_id                   bigint,
  provider_user_id          varchar(255),
  provider_key              varchar(255),
  constraint pk_linked_account primary key (id))
;

create table property (
  property_id               integer auto_increment not null,
  name                      varchar(255),
  address                   varchar(255),
  city                      varchar(255),
  state                     varchar(255),
  offering_size             integer,
  shares_available          integer,
  summary                   varchar(255),
  description               varchar(255),
  tenant_summary            varchar(255),
  market_summary            varchar(255),
  sponsor_id                integer,
  grade                     varchar(255),
  deal_type                 varchar(255),
  end_date                  datetime(6),
  irr                       double,
  rate                      double,
  initial_shares            integer,
  link                      varchar(255),
  fund_america_id           varchar(255),
  capital_rating            varchar(255),
  location_rating           varchar(255),
  occupancy_rating          varchar(255),
  sponsor_rating            varchar(255),
  personal_guarantee_rating varchar(255),
  property_guarantee_rating varchar(255),
  constraint pk_property primary key (property_id))
;

create table property_deal_points (
  property_id               integer,
  name                      varchar(255),
  description               varchar(255))
;

create table rate_return (
  property_id               integer,
  term                      datetime(6),
  estimate_return           double,
  actual_return             double)
;

create table security_role (
  id                        bigint auto_increment not null,
  role_name                 varchar(255),
  constraint pk_security_role primary key (id))
;

create table sponsor (
  sponsor_id                integer auto_increment not null,
  name                      varchar(255),
  website                   varchar(255),
  address                   varchar(255),
  description               varchar(255),
  constraint pk_sponsor primary key (sponsor_id))
;

create table token_action (
  id                        bigint auto_increment not null,
  token                     varchar(255),
  target_user_id            bigint,
  type                      varchar(2),
  created                   datetime(6),
  expires                   datetime(6),
  constraint ck_token_action_type check (type in ('PR','EV')),
  constraint uq_token_action_token unique (token),
  constraint pk_token_action primary key (id))
;

create table users (
  id                        bigint auto_increment not null,
  email                     varchar(255),
  name                      varchar(255),
  first_name                varchar(255),
  last_name                 varchar(255),
  phone_number              varchar(255),
  last_login                datetime(6),
  last_accessed             datetime(6),
  active                    tinyint(1) default 0,
  email_validated           tinyint(1) default 0,
  constraint pk_users primary key (id))
;

create table user_permission (
  id                        bigint auto_increment not null,
  value                     varchar(255),
  constraint pk_user_permission primary key (id))
;


create table users_security_role (
  users_id                       bigint not null,
  security_role_id               bigint not null,
  constraint pk_users_security_role primary key (users_id, security_role_id))
;

create table users_user_permission (
  users_id                       bigint not null,
  user_permission_id             bigint not null,
  constraint pk_users_user_permission primary key (users_id, user_permission_id))
;
alter table linked_account add constraint fk_linked_account_user_1 foreign key (user_id) references users (id) on delete restrict on update restrict;
create index ix_linked_account_user_1 on linked_account (user_id);
alter table token_action add constraint fk_token_action_targetUser_2 foreign key (target_user_id) references users (id) on delete restrict on update restrict;
create index ix_token_action_targetUser_2 on token_action (target_user_id);



alter table users_security_role add constraint fk_users_security_role_users_01 foreign key (users_id) references users (id) on delete restrict on update restrict;

alter table users_security_role add constraint fk_users_security_role_security_role_02 foreign key (security_role_id) references security_role (id) on delete restrict on update restrict;

alter table users_user_permission add constraint fk_users_user_permission_users_01 foreign key (users_id) references users (id) on delete restrict on update restrict;

alter table users_user_permission add constraint fk_users_user_permission_user_permission_02 foreign key (user_permission_id) references user_permission (id) on delete restrict on update restrict;

# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table capitalization;

drop table capitalization_type;

drop table document;

drop table investment;

drop table linked_account;

drop table property;

drop table property_deal_points;

drop table rate_return;

drop table security_role;

drop table sponsor;

drop table token_action;

drop table users;

drop table users_security_role;

drop table users_user_permission;

drop table user_permission;

SET FOREIGN_KEY_CHECKS=1;

